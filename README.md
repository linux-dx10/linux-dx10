# linux-dx10

Built from netboot with only the required packages to run XFCE efficiently, nothing else. This leaves room for your demanding apps and work process. Designed to stay out of your way and let you get things done. Add to that a limited but useful collec